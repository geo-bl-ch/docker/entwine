FROM registry.gitlab.com/geo-bl-ch/docker/pdal:v2.0.1

#
# Install entwine
#

USER root

ARG ENTWINE_VERSION=2.1.0
RUN apk --update add curl \
        openssl && \
    apk --update add --virtual .entwine-deps \
        make \
        cmake \
        gcc \
        g++ \
        file \
        git \
        autoconf \
        automake \
        curl-dev \
        openssl-dev \
        libtool && \
    cd /tmp && \
    find / -name "ZstdCompression" && \
    git clone https://github.com/connormanning/entwine.git && \
    cd entwine && \
    git checkout ${ENTWINE_VERSION} && \
    mkdir build && \
    cd build && \
    cmake -G "Unix Makefiles" \
        -DPDAL_LIBRARIES=/usr/local/lib \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_BUILD_TYPE=Release .. && \
    make && \
    make install && \
    cd ~ && \
    apk del .entwine-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

RUN apk --update add bash

RUN mkdir -p /data && \
    chown 1001 /data && \
    chgrp 0 /data && \
    chmod g=u /data && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd

RUN entwine || true

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/



WORKDIR /data

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["--version"]
